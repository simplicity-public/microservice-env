import waitPort from 'wait-port';
import { connect } from 'amqplib';

const { hostname, port } = new URL(process.env.MESSAGE_QUEUE_URL);
const exchangeName = process.env.EXCHANGE_NAME;
const queueName = process.env.SERVICE_NAME;

const start = async () => {
    const rabbitOpen = await waitPort({ host: hostname, port: Number(port) });
    if (!rabbitOpen) throw Error('Cannot start service');
    const connection = await connect(process.env.MESSAGE_QUEUE_URL);
    const channel = await connection.createChannel();
    await channel.consume(queueName, (message) => {
        const command = message.content.toString();
        switch (command) {
            case 'introduce_yourself':
                console.log('My name is microservice-1');
                break;
            case 'introduce_your_friend':
                console.log("My friend's name is");
                channel.publish(exchangeName, 'microservice-2', Buffer.from('introduce_yourself'));
                break;
        }
        channel.ack(message);
    });
};

start();
