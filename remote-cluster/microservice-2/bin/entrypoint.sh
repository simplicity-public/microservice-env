#!/bin/sh

if [ ! -d node_modules ]; then
    echo "node_modules missing, installing dependencies..."
    npm install
fi

if [ $DEV = "1" ]; then
    npm run dev
else
    npm run build
    npm start
fi
